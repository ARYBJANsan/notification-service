from rest_framework import viewsets
from client import models, serializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = models.Client.objects.all()
    serializer_class = serializer.ClientSerializer


class OperatorCodeViewSet(viewsets.ModelViewSet):
    queryset = models.OperatorCode.objects.all()
    serializer_class = serializer.OperatorCodeSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = models.Tag.objects.all()
    serializer_class = serializer.TagSerializer
