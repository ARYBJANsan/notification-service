from django.db import models
from django.utils.translation import gettext_lazy as _


class Client(models.Model):
    phone_number = models.CharField(_("Номер телефона"), max_length=9)
    operator_code = models.ForeignKey("client.OperatorCode", on_delete=models.CASCADE)
    tag = models.ForeignKey("client.Tag", on_delete=models.SET_NULL, null=True)
    time_zone = models.CharField(_("Часовой пояс"), max_length=7)


class OperatorCode(models.Model):
    code = models.CharField(_("Код оператора"), max_length=4)
    country = models.CharField(_("Страна"), max_length=100)


class Tag(models.Model):
    title = models.CharField(_("Теги"), max_length=150)
