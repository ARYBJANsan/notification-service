from django.contrib import admin
from client import models


class ClientAdmin(admin.ModelAdmin):
    list_display = ["id", "phone_number", "operator_code",
                    "tag", "time_zone"]


admin.site.register(models.Client, ClientAdmin)


class OperatorCodeAdmin(admin.ModelAdmin):
    list_display = ["id", "code", "country"]


admin.site.register(models.OperatorCode, OperatorCodeAdmin)


class TagAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


admin.site.register(models.Tag, TagAdmin)
