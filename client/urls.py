from rest_framework.routers import DefaultRouter
from client import views

router = DefaultRouter()

router.register("client", views.ClientViewSet)
router.register("operator-code", views.OperatorCodeViewSet)
router.register("tags", views.TagViewSet)
urlpatterns = router.urls
