from rest_framework import serializers
from client import models


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        models = models.Client
        fields = "__all__"


class OperatorCodeSerializer(serializers.ModelSerializer):
    class Meta:
        models = models.OperatorCode
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        models = models.Tag
        fields = "__all__"
