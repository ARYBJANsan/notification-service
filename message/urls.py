from rest_framework.routers import DefaultRouter
from message import views

router = DefaultRouter()

router.register("mailing", views.MailingViewSet)
router.register("message", views.MessageViewSet)
router.register("status", views.StatusViewSet)

urlpatterns = router.urls
