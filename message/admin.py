from django.contrib import admin
from message.models import Mailing, Message, Status


class MailingAdmin(admin.ModelAdmin):
    list_display = ["id", "start_date", "text_for_client",
                    "end_date", "tag", "phone_code"]


admin.site.register(Mailing, MailingAdmin)


class MessageAdmin(admin.ModelAdmin):
    list_display = ["id", "created_at", "status", "mailing", "client"]


admin.site.register(Message, MessageAdmin)


class StatusAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


admin.site.register(Status, StatusAdmin)
