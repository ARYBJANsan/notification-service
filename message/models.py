from django.db import models
from django.utils.translation import gettext_lazy as _


class Mailing(models.Model):
    start_date = models.DateTimeField(_("Начало рассылки"))
    text_for_client = models.TextField(_("Текст для клиента"))
    end_date = models.DateTimeField(_("Конец рассылки"))
    tag = models.ForeignKey("client.Tag", on_delete=models.CASCADE)
    phone_code = models.ForeignKey("client.OperatorCode", on_delete=models.CASCADE)


class Message(models.Model):
    created_at = models.DateTimeField(_("Дата отправки"))
    status = models.ForeignKey("message.Status", on_delete=models.CASCADE)
    mailing = models.ForeignKey("message.Mailing", on_delete=models.CASCADE)
    client = models.ForeignKey("client.Client", on_delete=models.CASCADE)


class Status(models.Model):
    title = models.CharField(_("Статус"), max_length=150)
