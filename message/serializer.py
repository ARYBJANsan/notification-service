from rest_framework import serializers
from message.models import Mailing, Message, Status


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        models = Mailing
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        models = Message
        fields = "__all__"


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        models = Status
        fields = "__all__"
